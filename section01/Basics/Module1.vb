﻿Module Module1

    Sub Main()
        Console.WriteLine("Expression: PAT1khg Like B?T#*: " & ("PAT1khg" Like "P?T#*"))
        Console.WriteLine("Expression: ABC123PQR Like ?B?###*: " & ("ABC123PQR" Like "?B?###*"))
        Console.WriteLine("Expression: 123A Like ###?: " & ("123A" Like "###?"))
        Console.WriteLine("Expression: 123Aaaa Like #?#*: " & ("123Aaaa" Like "#?#*"))
        Console.WriteLine("Expression: 754a687 Like ###?###: " & ("754a687" Like "###?###"))
    End Sub

End Module
