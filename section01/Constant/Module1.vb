﻿Module Module1

    Sub Main()
        Const pie = 3.1415
        Dim radius As Double
        Dim area As Double
        radius = 0
        area = 0
        Console.WriteLine("Enter Radius of circle")
        radius = Console.Read()
        area = pie * radius * radius
        Console.WriteLine("Area of Circle is: " & area)
    End Sub

End Module
