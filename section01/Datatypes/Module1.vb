﻿Module Module1

    Sub Main()
        Dim ch As Char
        ch = "G"
        System.Console.WriteLine("Char : " & ch)

        Dim str As String = "Batman"
        System.Console.WriteLine("String " & str)
        Console.WriteLine("{0} String ", str)

        Dim no1, no2, no3 As Integer
        ' Explicit inintilization is not permitted with multible values declaration with a single toye.
        Console.WriteLine("Integers: {0} {1} {2}", no1, no2, no3)

        Dim d As Double
        d = 10.1
        Console.WriteLine("Double " & d)

        Dim d1 As Decimal
        d1 = 9.1234727004
        Console.WriteLine("Decimal " & d1)

        Dim s As Single
        s = 0.00000044
        Console.WriteLine("Single " & s)

        Dim obj As Object
        obj = str
        Console.WriteLine("Object " & obj)

        obj = d
        Console.WriteLine("Object: " & obj)


    End Sub

End Module