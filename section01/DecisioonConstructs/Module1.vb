﻿Module Module1

    Sub Main()

        Dim answer As String
        answer = "yes"

        Dim sale As String
        sale = "On"

        Dim selling_price As Integer
        selling_price = 100
        Dim cost_price As Integer
        cost_price = 120

        Dim profit As Integer
        Dim loss As Integer
        Dim age As Integer
        Dim grade As Char

        ' basic if statement
        If answer = "yes" Then
            System.Console.WriteLine("Answer is yes")
        End If

        ' basic if else statement
        If sale = "On" Then
            Console.WriteLine("Discout 5 %")
        Else
            Console.WriteLine("Discout 0 %")
        End If

        ' basic else statement
        If selling_price > cost_price Then
            profit = selling_price - cost_price
            Console.WriteLine("Profit is :" & profit)
        Else
            loss = cost_price - selling_price
            Console.WriteLine("Loss is : " & loss)
        End If

        'basic if else statement with user input
        Console.WriteLine("Enter your age :")
        age = System.Console.ReadLine()
        If age >= 21 Then
            Console.WriteLine("The user is an adult")
        Else
            Console.WriteLine("The user is a minor")
        End If

        'basic elseif example
        Console.WriteLine("Enter grade")
        grade = Console.ReadLine()
        If grade = "A" Then
            Console.WriteLine("Commission = 2000")
        ElseIf grade = "B" Then
            Console.WriteLine("Commission = 1000")
        Else
            Console.WriteLine("Commission = 500")
        End If


    End Sub
End Module
