﻿Module Module1

    Sub Main()
        Dim num1 As Boolean
        Dim num2 As Boolean

        num1 = False
        num2 = True
        Console.WriteLine(num1 And num2)
        Console.WriteLine(num1 Or num2)
        Console.WriteLine(Not num1)

        Dim n1 As Integer, n2 As Integer
        n1 = 10
        n2 = 20

        Console.WriteLine("Expression: 10 > 20: " & (10 > 20))
        Console.WriteLine("Expression: 10 < 20: " & (10 < 20))
        Console.WriteLine("Expression 10 >= 20: " & (10 >= 20))
        Console.WriteLine("Expression 10 <= 20: " & (10 <= 20))
        Console.WriteLine("Expression 10 <> 20: " & (10 <> 20))
        Console.WriteLine("Expression 10 = 20: " & (10 = 20))

        Dim myObject
        Dim otherObject As New Object()
        Dim yourObject, thisObject, thatObject As Object
        yourObject = myObject
        thisObject = myObject
        thatObject = otherObject


        Console.WriteLine("Expression: yourObject Is thisobject :" & (yourObject Is thisObject))
        Console.WriteLine("Expression: thatObject IS thisObject :" & (thatObject Is thisObject))
        Console.WriteLine("Expression: myObject Is thatObject :" & (myObject Is thatObject))

        Console.WriteLine("Expression: F like F :" & ("F" Like "F"))
        Console.WriteLine("Expression: F like f : " & ("F" Like "f"))
        Console.WriteLine("Expression: F like FFF :" & ("F" Like "FFF"))

        Console.WriteLine("Expression: aBBBa Like a*a : " & ("aBBBa" Like "a*a"))

        Console.WriteLine("Expression: F like [A-Z] : " & ("F" Like "[A-Z]"))

        Console.WriteLine("Expression: a2a like a#a : " & ("a2a" Like "a#a"))

        Console.WriteLine("Expression: aM5b like a[L-P]#[!c-e]: " & ("aM5b" Like "a[L-P]#[!c-e]"))

        Console.WriteLine("Expression: BAT123kgh like B?T* :" & ("BAT123khg" Like "B?T*"))

        Console.WriteLine("Expression: CAT123kgh Like B?T* : " & ("BAT123khg" Like "B?T*"))

    End Sub

End Module
