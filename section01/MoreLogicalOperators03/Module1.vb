﻿
Option Strict On

'Option Strict on disallow implicit conversations from Double to Integer
'Default- off
'can be used in place of Options Expicit.
'Puts an ent to Evil type coersion
'Allows to cast downward but not upward
'Cannot perform late binding in a project that uses Options Strict
Module Module1

    Sub Main()
        Dim no1 As Integer
        Dim no2 As Double

        no1 = 110
        no2 = 20.3

        Console.WriteLine("Integer: " & no1)
        Console.WriteLine("Double : " & no2)

        no1 = CType(no2, Integer)
        Console.WriteLine("After conversions- form double to integer")
        Console.WriteLine("Integer" & no1)
        Console.WriteLine("Double" & no2)

    End Sub

End Module
