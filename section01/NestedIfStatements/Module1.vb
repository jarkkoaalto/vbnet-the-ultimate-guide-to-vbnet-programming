﻿Module Module1

    Sub Main()
        Dim gender As Char
        Dim age As Integer
        Dim n As Integer
        Dim m As Integer

        Console.WriteLine("Enter gender M ob W")
        gender = Console.ReadLine()
        Console.WriteLine("Enter your age")
        age = Console.ReadLine()

        If gender = "M" Then
            If age > 18 Then
                Console.WriteLine("Admission granted")
            Else
                Console.WriteLine("Admission not granted .. too young!")
            End If
        Else
            Console.WriteLine("Addmission denied")
        End If

        ' Example select statement
        Console.WriteLine("Enter a day of the week in numbers")
        n = Console.ReadLine()
        Select Case n
            Case 1
                Console.WriteLine("Monday")
            Case 2
                Console.WriteLine("Tuesday")
            Case 3
                Console.WriteLine("Wednesday")
            Case 4
                Console.WriteLine("Thursday")
            Case 5
                Console.WriteLine("Firday")
            Case 6
                Console.WriteLine("Saturday")
            Case 0
                Console.WriteLine("Sunday")
        End Select

        'Another select example
        Console.WriteLine("Enter marks scored")
        m = Console.ReadLine()
        Select Case m
            Case 0 To 38
                Console.WriteLine("Fail")
            Case 39 To 59
                Console.WriteLine("Pass Class")
            Case 60 To 97
                Console.WriteLine("Good")
            Case 98, 99, 100
                Console.WriteLine("Excelent")
        End Select



    End Sub

End Module
