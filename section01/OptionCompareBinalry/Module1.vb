﻿Option Compare Binary
' Option Compare Text
' Binary 
' Results in string comparisons based on a sort order derived form the 
' internal binary representations of the characters
' A<B<E<Z<a<b<e<z
' Text
'result in string comparisons on a case-insensitive text sort order determined by
' your system's locale.
' (A=a) < (B = b) < (E=e) < (Z=z)

Imports System

Module Module1

    Sub Main()
        Dim str As String = "AAA"
        Dim str1 As String = "aaa"

        If (str = str1) Then
            Console.WriteLine("Equals - Text")
        Else
            Console.WriteLine("UnEqual - Binary")
        End If
    End Sub

End Module
