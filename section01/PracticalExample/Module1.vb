﻿Module Module1

    Sub Main()
        Dim x As Integer
        x = 1
        While x < 10 ' While: Condition check first 
            Console.Write(x)
            x = x + 1
        End While

        Console.WriteLine("")

        Dim y As Integer
        y = 1
        Do ' Do while: condition chech after first print
            Console.Write(y)
            y = y + 1
        Loop While y < 10

        Console.WriteLine("")

        Dim f As Integer
        For f = 1 To 9
            Console.Write(f)
        Next

        Console.WriteLine("")

        Dim i As Integer
        Dim j As Integer
        For i = 1 To 10
            For j = 1 To i
                Console.Write("*")
            Next j
            Console.WriteLine("")
        Next i

        Dim k As Integer
        k = 10
        While k < 100
            Console.WriteLine(k)
            If k Mod 7 = 0 Then
                Exit While
            End If
            k = k + 1
        End While

        Console.WriteLine("")

        Dim h As Integer
        Dim l As Integer
        For h = 1 To 5
            For l = 1 To 3
                Console.WriteLine("h is " & h & " l is " & l)
                If ((h + l) > 4) Then
                    Exit For
                End If

            Next l
            Console.WriteLine("end")
        Next h
        Console.WriteLine("The end")

    End Sub
End Module
