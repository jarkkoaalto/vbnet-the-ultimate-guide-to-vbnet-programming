﻿Module Module1

    Sub Main()
        Dim num1 As Long
        Dim num2 As Double
        num1 = 500
        num2 = CType(num1, Double)
        Console.WriteLine("Long Value: " & num1)
        Console.WriteLine("Double Value: " & num2)
    End Sub

End Module
