﻿Module Module1

    Sub Main()
        Dim n1 As Integer
        Dim quotient As Decimal
        n1 = 5
        quotient = n1 / 2
        Console.WriteLine(quotient)

        Dim a As Single
        Dim b As Single
        a = 5.5
        a = CType(b, Single)
        'had the ctype function not been used the program would not'
        'have compiled'
        Console.WriteLine(a)
    End Sub

End Module
