Imports System

Module Program
    Sub Main()
        Dim i As Integer
        i = 10
        While i < 100
            Console.WriteLine(i)
            If i Mod 7 = 0 Then
                Exit While
            End If
            i = i + 1
        End While
    End Sub
End Module
