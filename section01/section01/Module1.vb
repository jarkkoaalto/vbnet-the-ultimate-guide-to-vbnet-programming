﻿Module Module1

    Sub Main()
        Dim a As Integer
        Dim b As Decimal
        Dim c As Char
        Dim d As Single
        a = 100
        b = 9.500342
        c = "M"
        d = 0.00003555
        System.Console.WriteLine("Integer " & a)
        System.Console.WriteLine("Decimal " & b)
        System.Console.WriteLine("Char " & c)
        System.Console.WriteLine("Single " & d)
    End Sub

End Module
