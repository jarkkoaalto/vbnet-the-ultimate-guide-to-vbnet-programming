﻿Module Module1

    Sub Main()
        Dim a As Char


        Console.WriteLine("Enter attendance")
        Console.WriteLine("R for Regular, A for absentee and I for irregular")
        a = Console.ReadLine()
        total(a)

    End Sub

    Sub total(x As Char)
        Dim ave As Single
        Dim marks1 As Integer
        Dim marks2 As Integer
        Dim marks3 As Integer

        If x = "A" Or x = "I" Then
            Console.WriteLine("Absentee or irregular hence no result calculated")
            Exit Sub ' This terminated A and I options
        End If

        Console.WriteLine("Enter 3 marks")
        marks1 = Console.ReadLine()
        marks2 = Console.ReadLine()
        marks3 = Console.ReadLine()
        ave = (marks1 + marks2 + marks3) / 3
        Console.WriteLine("Average marks is " & ave)

    End Sub


End Module
