Imports System

Module Program
    Sub Main(args As String())
        Dim a As Integer
        Dim b As Integer
        Dim c As Decimal
        Console.WriteLine("Enter values of a and b")
        a = Console.ReadLine()
        b = Console.ReadLine()
        Try
            c = a / b
            Console.WriteLine(c)
        Catch s As System.Exception
            Console.WriteLine("Error ... divide by zero")
        Finally
            Console.WriteLine("End of main")
        End Try
    End Sub
End Module
