﻿Module Module1

    Sub Main()
        Dim num As Integer
        Dim f As Long
        System.Console.WriteLine("Enter number")
        num = System.Console.ReadLine()
        f = factorial(num)
        System.Console.WriteLine("Factorial is " & f)
    End Sub

    Function factorial(x As Integer)
        Dim fact As Long
        Dim i As Integer
        fact = 1
        For i = 1 To x
            fact = fact * i
        Next i
        Return fact
    End Function

End Module
