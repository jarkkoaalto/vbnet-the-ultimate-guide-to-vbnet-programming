﻿Module Module1
    Sub increment(ByRef x As Integer)
        x = x + 2
        System.Console.WriteLine("The value is {0}", x)
    End Sub
    Sub Main()
        Dim n As Integer
        n = 10
        System.Console.WriteLine("The value within main procedure is {0}", n)
        increment(n)
        System.Console.WriteLine("The value with main procedure after calling increment procedure is {0}", n)
    End Sub

End Module
