﻿Module Module1
    Sub notify(ByVal company As String, Optional ByVal location As String = "None")
        If location = "None" Then
            System.Console.WriteLine("Location not supplied hence assumed to be Palo Alto, CA")
            location = "Palo Alto, CA"
        Else
            System.Console.WriteLine("Company is {0}", company)
            System.Console.WriteLine("Location is {0}", location)
        End If
    End Sub

    Sub Main()
        notify("Vb.NET", "London")
    End Sub

End Module
