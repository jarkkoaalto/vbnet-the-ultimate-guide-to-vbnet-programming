Imports System

Module Program
    Sub increment(ByRef x As Integer)
        x = x + 2
        Console.WriteLine("The value is " & x)
    End Sub

    Sub Main()
        Dim n As Integer
        n = 10
        Console.WriteLine("The value within procedures is " & n)
        increment(n)
        Console.WriteLine("The value in main is " & n)
    End Sub
End Module
