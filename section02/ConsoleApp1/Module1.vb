﻿Module Module1

    Sub Main()
        Dim j As Integer

        System.Console.WriteLine("Enter the value to calculate square")
        j = System.Console.ReadLine()
        square(j)
        System.Console.WriteLine("End of main program")
    End Sub

    Sub square(x As Integer) 'prcedure definition
        System.Console.WriteLine(x * x)
    End Sub

End Module
