﻿Module Module1

    Sub Main()
        Dim num As Integer
        Dim f As Long
        Console.WriteLine("Enter the number")
        num = Console.ReadLine()
        f = factorial(num)
        Console.WriteLine("factorial is " & f)
    End Sub

    Function factorial(x As Integer)
        Dim fact As Long
        Dim i As Integer
        fact = 1
        For i = 1 To x
            fact = fact * i
        Next i
        Return fact
    End Function
End Module
