Imports System

Module Program
    Sub Main(args As String())
        Dim a As Short
        Dim b As Long

        a = 5000
        b = 50000
        Try
            a = CType(b, Short)
            Console.WriteLine(a)
        Catch s As System.Exception
            Console.WriteLine("Exception occured")
            Console.WriteLine(s)
        Catch s As System.OverflowException
            Console.WriteLine("Error ... value exceeds range of short")

        End Try
    End Sub
End Module
