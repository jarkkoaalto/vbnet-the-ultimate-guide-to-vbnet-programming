Imports System

Module Program
    Sub Main(args As String())
        Dim a As Short
        Dim b As Long

        a = 5000
        b = 50000
        Try
            a = CShort(b)
            Console.WriteLine(a)
        Catch s As OverflowException
            Console.WriteLine("Error ... value exceed range of short")
        Catch s As System.Exception
            Console.WriteLine("Exception occured")
        End Try
    End Sub
End Module
