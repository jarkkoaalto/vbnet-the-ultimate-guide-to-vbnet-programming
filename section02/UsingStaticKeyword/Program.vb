Imports System

Module Program
    Sub Main(args As String())
        Console.WriteLine("First call")
        print1()
        Console.WriteLine("Second call")
        print1()
        Console.WriteLine("Third call")
        print1()
    End Sub

    Sub print1()
        Static i As Integer
        i = i + 1
        Console.WriteLine(i)
    End Sub

End Module
