﻿Module Module1

    Sub Main()
        Dim j As Integer
        Console.WriteLine("Enter  number:")
        j = Console.ReadLine()
        Square(j) ' rpocedure call
        Console.WriteLine("End of main program")
    End Sub

    Sub Square(x As Integer)
        Console.WriteLine(x * x)
    End Sub

End Module
